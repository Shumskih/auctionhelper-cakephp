<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\BiddersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\BiddersTable Test Case
 */
class BiddersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\BiddersTable
     */
    public $Bidders;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.bidders'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Bidders') ? [] : ['className' => BiddersTable::class];
        $this->Bidders = TableRegistry::getTableLocator()->get('Bidders', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Bidders);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
