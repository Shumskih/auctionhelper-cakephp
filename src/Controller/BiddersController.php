<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Bidders Controller
 *
 * @property \App\Model\Table\BiddersTable $Bidders
 *
 * @method \App\Model\Entity\Bidder[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class BiddersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $bidders = $this->paginate($this->Bidders);

        $this->set(compact('bidders'));
    }

    /**
     * View method
     *
     * @param string|null $id Bidder id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bidder = $this->Bidders->get($id, [
            'contain' => []
        ]);

        $this->set('bidder', $bidder);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bidder = $this->Bidders->newEntity();
        if ($this->request->is('post')) {
            $bidder = $this->Bidders->patchEntity($bidder, $this->request->getData());
            if ($this->Bidders->save($bidder)) {
                $this->Flash->success(__('The bidder has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bidder could not be saved. Please, try again.'));
        }
        $this->set(compact('bidder'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Bidder id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bidder = $this->Bidders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bidder = $this->Bidders->patchEntity($bidder, $this->request->getData());
            if ($this->Bidders->save($bidder)) {
                $this->Flash->success(__('The bidder has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The bidder could not be saved. Please, try again.'));
        }
        $this->set(compact('bidder'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Bidder id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bidder = $this->Bidders->get($id);
        if ($this->Bidders->delete($bidder)) {
            $this->Flash->success(__('The bidder has been deleted.'));
        } else {
            $this->Flash->error(__('The bidder could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
