<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Bidder Entity
 *
 * @property int $bidderid
 * @property string $lastname
 * @property string $firstname
 * @property string $address
 * @property string $phone
 */
class Bidder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'lastname' => true,
        'firstname' => true,
        'address' => true,
        'phone' => true
    ];
}
