<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Bidders Model
 *
 * @method \App\Model\Entity\Bidder get($primaryKey, $options = [])
 * @method \App\Model\Entity\Bidder newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Bidder[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Bidder|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bidder|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Bidder patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Bidder[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Bidder findOrCreate($search, callable $callback = null, $options = [])
 */
class BiddersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('bidders');
        $this->setDisplayField('bidderid');
        $this->setPrimaryKey('bidderid');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('bidderid')
            ->allowEmpty('bidderid', 'create');

        $validator
            ->scalar('lastname')
            ->maxLength('lastname', 100)
            ->allowEmpty('lastname');

        $validator
            ->scalar('firstname')
            ->maxLength('firstname', 100)
            ->allowEmpty('firstname');

        $validator
            ->scalar('address')
            ->maxLength('address', 200)
            ->allowEmpty('address');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 14)
            ->allowEmpty('phone');

        return $validator;
    }
}
