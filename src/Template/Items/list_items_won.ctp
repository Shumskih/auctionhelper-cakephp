<div class="items view large-9 medium-8 columns content">
<?php echo "<h3>Items won by Bidder $winbidder[0]</h3>\n"; ?>
<section>
<?php $total = 0; ?>
<table>
<tr><th>ItemID</th><th>Name</th><th>Description</th><th>Winning Bid
</th></tr>
<?php foreach ($items as $item): ?>
<?php $total = $total + $item->winprice; ?>
<?php echo "<tr><td>$item->itemid</td><td>$item->name</td>" ?>
<?php echo "<td>$item->description</td><td>$item->winprice</td>
</tr>\n"; ?>
<?php endforeach; ?>
<?php echo "<tr><td/><td/><td>Total</td><td>$total</td>\n"; ?>
</table>
<?= $this->Html->link(__('Return to Bidders list'),['controller' =>
'bidders', 'action' => 'index']) ?>
</section>