<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Item $item
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Item'), ['action' => 'edit', $item->itemid]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Item'), ['action' => 'delete', $item->itemid], ['confirm' => __('Are you sure you want to delete # {0}?', $item->itemid)]) ?> </li>
        <li><?= $this->Html->link(__('List Items'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Item'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="items view large-9 medium-8 columns content">
    <h3><?= h($item->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($item->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Itemid') ?></th>
            <td><?= $this->Number->format($item->itemid) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Resaleprice') ?></th>
            <td><?= $this->Number->format($item->resaleprice) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Winbidder') ?></th>
            <td><?= $this->Number->format($item->winbidder) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Winprice') ?></th>
            <td><?= $this->Number->format($item->winprice) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($item->description)); ?>
    </div>
</div>
