<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bidder[]|\Cake\Collection\CollectionInterface $bidders
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Bidder'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="bidders index large-9 medium-8 columns content">
    <h3><?= __('Bidders') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('bidderid') ?></th>
                <th scope="col"><?= $this->Paginator->sort('lastname') ?></th>
                <th scope="col"><?= $this->Paginator->sort('firstname') ?></th>
                <th scope="col"><?= $this->Paginator->sort('address') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($bidders as $bidder): ?>
            <tr>
                <td><?= $this->Number->format($bidder->bidderid) ?></td>
                <td><?= h($bidder->lastname) ?></td>
                <td><?= h($bidder->firstname) ?></td>
                <td><?= h($bidder->address) ?></td>
                <td><?= h($bidder->phone) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Items'), ['controller' => 'items', 'action' => 'listItemsWon', $bidder->bidderid]) ?>
                    <?= $this->Html->link(__('View'), ['action' => 'view', $bidder->bidderid]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $bidder->bidderid]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $bidder->bidderid], ['confirm' => __('Are you sure you want to delete # {0}?', $bidder->bidderid)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
