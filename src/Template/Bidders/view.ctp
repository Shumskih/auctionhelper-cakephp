<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bidder $bidder
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Bidder'), ['action' => 'edit', $bidder->bidderid]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Bidder'), ['action' => 'delete', $bidder->bidderid], ['confirm' => __('Are you sure you want to delete # {0}?', $bidder->bidderid)]) ?> </li>
        <li><?= $this->Html->link(__('List Bidders'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Bidder'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="bidders view large-9 medium-8 columns content">
    <h3><?= h($bidder->bidderid) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Lastname') ?></th>
            <td><?= h($bidder->lastname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Firstname') ?></th>
            <td><?= h($bidder->firstname) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Address') ?></th>
            <td><?= h($bidder->address) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone') ?></th>
            <td><?= h($bidder->phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Bidderid') ?></th>
            <td><?= $this->Number->format($bidder->bidderid) ?></td>
        </tr>
    </table>
</div>
