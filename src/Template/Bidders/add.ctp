<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Bidder $bidder
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Bidders'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="bidders form large-9 medium-8 columns content">
    <?= $this->Form->create($bidder) ?>
    <fieldset>
        <legend><?= __('Add Bidder') ?></legend>
        <?php
            echo $this->Form->control('lastname');
            echo $this->Form->control('firstname');
            echo $this->Form->control('address');
            echo $this->Form->control('phone');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
